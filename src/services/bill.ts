import type { Bill } from '@/types/Bill'
import http from './http'

function addBill(bill: Bill) {
  return http.post('/bills', bill) // ส่งข้อมูลสมาชิกไปพร้อมกับคำขอ
}

function updateBill(bill: Bill) {
  return http.patch(`/bills/${bill.id}`, bill) // ใช้ http.patch เพื่อทำการอัปเดตข้อมูลสมาชิก
}

function delBill(bill: Bill) {
  return http.delete(`/bills/${bill.id}`)
}

function getBill(id: number) {
  return http.get(`/bills/${id}`)
}

function getBills() {
  return http.get('/bills')
}
function getAllsp() {
  return http.get(`/materials/sp`)
}

export default { addBill, updateBill, delBill, getBill, getBills, getAllsp }
