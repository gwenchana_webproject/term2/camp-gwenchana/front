import type { TypeBill } from '@/types/TypeBill'
import http from './http'

function addTypeBill(typebill: TypeBill) {
  return http.post('/typebills', typebill)
}

function updateTypeBill(typebill: TypeBill) {
  return http.patch(`/typebills/${typebill.id}`, typebill)
}

function delTypeBill(typebill: TypeBill) {
  return http.delete(`/typebills/${typebill.id}`)
}

function getTypeBill(id: number) {
  return http.get(`/typebills/${id}`)
}

function getTypeBills() {
  return http.get('/typebills')
}

export default { addTypeBill, updateTypeBill, delTypeBill, getTypeBill, getTypeBills }
