import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member) {
  return http.post('/members', member) // ส่งข้อมูลสมาชิกไปพร้อมกับคำขอ
}

function updateMember(member: Member) {
  return http.patch(`/members/${member.id}`, member) // ใช้ http.patch เพื่อทำการอัปเดตข้อมูลสมาชิก
}

function delMember(member: Member) {
  return http.delete(`/members/${member.id}`)
}

function getMember(id: number) {
  return http.get(`/members/${id}`)
}

function getMembers() {
  return http.get('/members')
}
function getMemberBytel(tel: string) {
  return http.get(`/members/${tel}`)
}

export default { addMember, updateMember, delMember, getMember, getMembers,getMemberBytel }
