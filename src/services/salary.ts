import type { Salary } from '@/types/Salary'
import http from './http'
import user from './user'

function addSalary(salary: Salary) {
    console.log('call addSalary from service')
    console.log(salary)
    return http.post('/salarys', salary)
}

function updateSalary(salary: Salary) {
    return http.patch(`/salary/${salary.id}`) 
      console.log('call addSalary from service')
      console.log(salary)
      return http.post('/salarys', salary)
  }

  function delSalary(salary: Salary) {
    return http.delete(`/salary/${salary.id}`)
  }
  
  function getSalary(id: number) {
    return http.get(`/salary/${id}`)
  }
  
  function getSalarys() {
    return http.get('/salary')
  }
  
  //function getSalaryById(Id: number) {
    //return http.get('/salary/empId/' + Id)
  //}
  
  export default { addSalary, updateSalary, delSalary, getSalary, getSalarys} //getSalaryById
  
