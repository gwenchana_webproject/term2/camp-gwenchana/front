import type { Checkin } from '@/types/Checkin'
import http from './http'
function getChecks() {
  return http.get("/checkinouts");
}

function saveCheck(check: Checkin) {
  return http.post("/checkinouts", check);
}

function updateCheck(id: number, check: Checkin) {
  return http.patch(`/checkinouts/${id}`, check);
}

function deleteCheck(id: number) {
  return http.delete(`/checkinouts/${id}`);
}

export default { getChecks, saveCheck, updateCheck, deleteCheck };

