import { ref, watch } from "vue";
import { defineStore } from "pinia";
import checkService from "@/services/checkin";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type { Checkin } from '@/types/Checkin'

export const useCheckStore = defineStore("Check", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const dialog1 = ref(false);
  const checks = ref<Checkin[]>([]);
  const date = ref("");
  const today = new Date();
  date.value =
    today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
  const editedCheck = ref<Checkin>({
    date: date.value,
    time_in: new Date().toISOString(),
    time_out: "",
    total_hour: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCheck.value = {
        date: date.value,
        time_in: new Date().toISOString(),
        time_out: "",
        total_hour: "",
      };
    }
  });
  async function getChecks() {
    loadingStore.isLoading = true;
    try {
      const res = await checkService.getChecks();
      checks.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Time ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCheck() {
    loadingStore.isLoading = true;
    try {
      if (editedCheck.value.id) {
        const res = await checkService.updateCheck(
          editedCheck.value.id,
          editedCheck.value
        );
        console.log("update")
      } else {
        const res = await checkService.saveCheck(editedCheck.value);
        
        console.log("save")
      }

      dialog.value = false;
      await getChecks();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Time ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

   function editCheck(check: Checkin) {
   
    const isCheckOut = check.time_out === undefined || check.time_out === null || check.time_out === "";

    // ปรับแก้ค่าของ time_out เป็นเวลาปัจจุบันเมื่อเป็นการ CHECK OUT
    if (isCheckOut) {
      check.time_out = new Date().toISOString();
     console.log("update")
      return check
    }
  }

  async function updateCheck(check: Checkin) {
    loadingStore.isLoading = true;
    try {
      // ปรับแก้ค่าของ time_out เป็นเวลาปัจจุบันเมื่อเป็นการ CHECK OUT
      if (!check.time_out) {
        check.time_out = new Date().toISOString();
      }
  
      // ตรวจสอบว่า check.time_in ไม่เป็น undefined ก่อนที่จะใช้งาน new Date()
      if (check.time_in) {
        const timeIn = new Date(check.time_in).getTime();
        const timeOut = new Date(check.time_out).getTime();
        const millisecondsInHour = 1000 * 60 * 60; // จำนวนมิลลิวินาทีในหนึ่งชั่วโมง
        const totalHour = Math.abs((timeOut - timeIn) / millisecondsInHour).toFixed(2); // คำนวณ total_hour
        check.total_hour = totalHour.toString(); // กำหนดค่า total_hour ใหม่
      }
  
      // อัปเดตข้อมูลผ่านทาง checkService
      await checkService.updateCheck(check.id, check);
      console.log(check)
  
      // เมื่ออัปเดตเสร็จสิ้น ปิดการโหลด
      loadingStore.isLoading = false;
    } catch (e) {
      // ในกรณีที่เกิดข้อผิดพลาด แสดงข้อความแจ้งเตือน
      messageStore.showMessage("ไม่สามารถบันทึก Time ได้");
      console.log(e);
      loadingStore.isLoading = false;
    }
  }
  
  
  

  return {
    checks,
    getChecks,
    dialog,
    editedCheck,
    editCheck,
    saveCheck,
    updateCheck,checkService
  };
});
