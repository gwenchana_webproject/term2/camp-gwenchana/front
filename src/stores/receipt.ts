import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import orderService from '@/services/order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { isTemplateExpression } from 'typescript'
import type { Product } from '@/types/Product'
import type { RefSymbol } from '@vue/reactivity'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptDialog = ref(false)
  const receiptItems = ref<ReceiptItem[]>([])
  const product = ref<Product[]>([])
  const receipt = ref<Receipt>()
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      amount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      memberId: -1,
      user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    {
      deep: true
    }
  )

  const calReceipt = function () {
    receipt.value!.total = 0
    receipt.value!.amount = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.total += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.amount += receiptItems.value[i].unit
    }
  }

  function addReceiptItem(newReceipt: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item.productId === newReceipt.productId)
    console.log(newReceipt.productId)
    if (index >= 0) {
      receiptItems.value[index].unit++
    } else {
      receiptItems.value.push(newReceipt)
    }
    calReceipt()
  }

  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
    calReceipt()
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    if (selectedItem.unit === 1) {
      deleteReceiptItem(selectedItem)
    }
    selectedItem.unit--
    calReceipt()
  }

  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  function showReceiptDialog() {
    receiptDialog.value = true
    // show rec
    receipt.receiptItems = receiptItems.value
  }

  return {
    receipt,
    receiptItems,
    initReceipt,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    order,
    showReceiptDialog
  }
})
