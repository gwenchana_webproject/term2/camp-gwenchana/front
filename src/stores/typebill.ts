import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import typebillService from '@/services/typebill'
import type { TypeBill } from '@/types/TypeBill'

export const useTypeBillStore = defineStore('typebill', () => {
  const loadingStore = useLoadingStore()
  const typebills = ref<TypeBill[]>([])
  const initialTypeBill: TypeBill = {
    name: ''
  }
  const editedTypeBill = ref<TypeBill>(JSON.parse(JSON.stringify(initialTypeBill)))

  async function getTypeBill(id: number) {
    loadingStore.doLoad()
    const res = await typebillService.getTypeBill(id)
    editedTypeBill.value = res.data
    loadingStore.finish()
  }
  async function getTypeBills() {
    try {
      loadingStore.doLoad()
      const res = await typebillService.getTypeBills()
      typebills.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveTypeBill() {
    loadingStore.doLoad()
    const typebill = editedTypeBill.value
    if (!typebill.id) {
      // Add new
      console.log('Post ' + JSON.stringify(typebill))
      const res = await typebillService.addTypeBill(typebill)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(typebill))
      const res = await typebillService.updateTypeBill(typebill)
    }

    await getTypeBills()
    loadingStore.finish()
  }
  async function deleteTypeBill() {
    loadingStore.doLoad()
    const typebill = editedTypeBill.value
    const res = await typebillService.delTypeBill(typebill)

    await getTypeBills()
    loadingStore.finish()
  }

  function clearForm() {
    editedTypeBill.value = JSON.parse(JSON.stringify(initialTypeBill))
  }
  return {
    typebills,
    getTypeBills,
    saveTypeBill,
    deleteTypeBill,
    editedTypeBill,
    getTypeBill,
    clearForm
  }
})
