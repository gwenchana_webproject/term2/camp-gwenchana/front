import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import billService from '@/services/bill'
import type { Bill } from '@/types/Bill'
import { useMessageStore } from './message'

export const useBillStore = defineStore('bill', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const date = ref('')
  const today = new Date()
  date.value = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear()
  const billSp = ref<Bill[]>([])
  const bills = ref<Bill[]>([])

  const initialBill: Bill = {
    dateAt: date.value,
    price: 0,
    typebill: { id: 0, name: '' }
  }
  const editedBill = ref<Bill>(JSON.parse(JSON.stringify(initialBill)))

  async function getBill(id: number) {
    try {
      loadingStore.doLoad()
      const res = await billService.getBill(id)
      editedBill.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getAllsp() {
    try {
      loadingStore.doLoad()
      const res = await billService.getAllsp()
      billSp.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
    console.log(billSp.value)
  }
  async function getBills() {
    try {
      loadingStore.doLoad()
      const res = await billService.getBills()
      bills.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveBill() {
    try {
      loadingStore.doLoad()
      const bill = editedBill.value
      if (!bill.id) {
        // Add new
        console.log('Post ' + JSON.stringify(bill))
        const res = await billService.addBill(bill)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(bill))
        const res = await billService.updateBill(bill)
      }

      await getBills()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteBill() {
    loadingStore.doLoad()
    const bill = editedBill.value
    const res = await billService.delBill(bill)

    await getBills()
    loadingStore.finish()
  }

  function clearForm() {
    editedBill.value = JSON.parse(JSON.stringify(initialBill))
  }
  return {
    bills,
    getBills,
    saveBill,
    deleteBill,
    editedBill,
    getBill,
    clearForm,
    getAllsp,
    billSp
  }
})
