import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import salaryService from '@/services/salary'
import http from '@/services/http'
import type { Salary } from '@/types/Salary'
import { useMessageStore } from './message'

export const useSalaryStore = defineStore('salary', () => {
    const loadingStore = useLoadingStore()
    const messageStore = useMessageStore()
    const salary = ref<Salary[]>([])
    const initialSalary: Salary  = {
        id: -1,
        //user: '',
        user_Id: -1,
        user_Name: '',
        user_BankAcc: '',
        salary: 0,
        bonus: 0,
        netSalary: 0,
        status: 'Pending',
        payDate: new Date
    },
        User = {
        email: '',
        password: '',
        fullName: '',
        gender: 'male',
        roles: [{ id: 2, name: 'user' }],
        //image: 'noimage.jpg',
        //files: []
      }

const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

async function getSalarys() {
    try {
    loadingStore.doLoad()
    const res = await salaryService.getSalarys()
    editedSalary.value = res.data
    console.log(res)
    console.log(salary.value)
    loadingStore.finish()
} catch (e: any) {
    loadingStore.finish()
    messageStore.showMessage(e.message)
  }
}
async function getSalary(id: number) {
    try {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
} catch (e: any) {
    loadingStore.finish()
    messageStore.showMessage(e.message)
  }
}
async function saveSalary() {
    // loadingStore.doLoad()
    // const salary = editedSalary.value
    console.log('call saveSalary...')
    // console.log(salary)
    //add new
    try {
        loadingStore.doLoad()
        const salary = editedSalary.value
        if (salary.id === -1) {
            //salary.id = salary.value.length + 1
            //salary.user_Id = salary.user_id//assign Id in salary
            //salary.user_Name = salary.user_name
            //salary.user_BankAcc = salary.user_bankAcc
            // Add new
            console.log('Post ' + JSON.stringify(salary))
                const res = await salaryService.addSalary(salary)
            } else {
                // Update
                console.log('Patch ' + JSON.stringify(salary))
                const res = await salaryService.updateSalary(salary)
                console.log(res)
            }

            await getSalarys()
            loadingStore.finish()
        } catch (e: any) {
            messageStore.showMessage(e.message)
            loadingStore.finish()
        }
    }
        async function deleteSalary() {
            loadingStore.doLoad()
            const salary = editedSalary.value
            const res = await salaryService.delSalary(salary)

            await getSalarys()
            loadingStore.finish()
        }

        function clearForm() {
            editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
        }

        async function paySalary() {
            loadingStore.doLoad()
            const salary = editedSalary.value
            salary.status = 'Paid'
            const res = await salaryService.updateSalary(salary)
            console.log('Call PaySalary...')
            console.log('Patch ' + JSON.stringify(salary))
            await getSalarys()
            loadingStore.finish()
        }
    
        function getStatusById(id: number): string | null {
            const foundSalary = salary.value.find(salary => salary.id === id)
            return foundSalary ? foundSalary.status : null
        }
    
        return { getSalary, getSalarys, saveSalary, deleteSalary, clearForm, salary, editedSalary, paySalary, getStatusById }
    })  
    
    