import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './message'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const members = ref<Member[]>([])
  const initialMember: Member  = {
    id: 0,
    name: '',
    tel: '',
    point: 0

  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))

  async function getMember(id: number) {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMember(id)
      editedMember.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  //รับข้อมูลเป็น tel string
  async function getMemberBytel(tel: string) {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMemberBytel(tel)
      editedMember.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        // Add new
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }

      await getMembers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)

    await getMembers()
    loadingStore.finish()
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }
  return { members, getMembers,getMemberBytel, saveMember, deleteMember, editedMember, getMember, clearForm }
})
