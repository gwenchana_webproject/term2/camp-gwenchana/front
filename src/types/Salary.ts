import type { User } from "./User"
type status = 'Paid' | 'Pending'

type Salary = {
    id: number
    //user: User
    user_Id: number
    user_Name: string
    user_BankAcc: string
    salary: number
    bonus: number
    netSalary: number
    status: status
    payDate: Date
  }

  export { type Salary, type status }