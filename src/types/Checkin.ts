
type Checkin = {
    id?: number;
    date?: String;
    time_in?: string;
    time_out?: string;
    total_hour?: string;
  
}
export type {Checkin}