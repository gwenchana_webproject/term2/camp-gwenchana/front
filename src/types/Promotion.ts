type Promotion = {
  id: number
  name: string
  condition: number
  discountPrice: number
}

export type { Promotion }
