import type { TypeBill } from './TypeBill'

type Bill = {
  id?: number
  dateAt: string
  price: number
  typebill: TypeBill
}

export { type Bill }
